from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math


def public_result(url, result):
    # Костыльный код для того чтобы опубликовать результат на степике автоматически
    try:
        browser = webdriver.Chrome()
        browser.implicitly_wait(10)
        # AUTH on stepik
        browser.get("https://stepik.org/catalog?auth=login")
        browser.find_element(By.ID, "id_login_email").send_keys("mocaceb482@d3bb.com")
        browser.find_element(By.ID, "id_login_password").send_keys("SuperTest")
        browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click()
        browser.find_element(By.ID, "ember22").click()
        browser.find_element(By.LINK_TEXT, "Профиль").click()
        # Страница урока
        browser.get(url)
        browser.find_element(By.CSS_SELECTOR, "[placeholder=\"Напишите ваш ответ здесь...\"]").send_keys(result)
        browser.find_element(By.CLASS_NAME, "submit-submission").click()
        a = browser.find_element(By.CLASS_NAME, "attempt-message_correct").text
        return(a)


    finally:
        browser.quit()

def alert_result(str):
    return str.split(': ')[-1]

def calc(x):  # Функция для подсчета
    return str(math.log(abs(12 * math.sin(int(x)))))
