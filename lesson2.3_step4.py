from selenium import webdriver
from selenium.webdriver.common.by import By
import time
from Stepik_PUBLIC_RESULT import alert_result
from Stepik_PUBLIC_RESULT import public_result
from Stepik_PUBLIC_RESULT import calc

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/alert_accept.html"
    browser.get(link)
    browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click()
    browser.switch_to.alert.accept()
    number = browser.find_element(By.ID, "input_value").text
    result = calc(number)
    browser.find_element(By.ID, "answer").send_keys(result)
    browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click()
    final_result = browser.switch_to.alert.text.split(": ")[-1]
    public_result("https://stepik.org/lesson/184253/step/4?unit=158843", final_result)

finally:
    time.sleep(5)
    browser.quit()