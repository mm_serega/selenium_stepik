from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from Stepik_PUBLIC_RESULT import public_result
from Stepik_PUBLIC_RESULT import calc
import time
'''
try:
    browser = webdriver.Chrome()
    browser.implicitly_wait(5)

    browser.get("http://suninjuly.github.io/wait1.html")

    button = browser.find_element(By.ID, "verify")
    button.click()
    message = browser.find_element(By.ID, "verify_message")

    assert "successful" in message.text

finally:
    browser.quit()
'''
'''
browser = webdriver.Chrome()
browser.implicitly_wait(5)

browser.get("http://suninjuly.github.io/wait2.html")

# говорим Selenium проверять в течение 5 секунд, пока кнопка не станет кликабельной
button = WebDriverWait(browser, 0.5).until(
        EC.element_to_be_clickable((By.ID, "verify"))
    ) # until_not - негативное ожидаение
button.click()
message = browser.find_element(By.ID, "verify_message")

assert "successful" in message.text
'''
try:
    browser = webdriver.Chrome()
    browser.implicitly_wait(5)

    browser.get("http://suninjuly.github.io/explicit_wait2.html")
    WebDriverWait(browser, 20).until(
        EC.text_to_be_present_in_element((By.ID, "price"), "100")
        )
    browser.find_element(By.ID, "book").click()

    number = browser.find_element(By.ID, "input_value").text
    result = calc(number)

    browser.find_element(By.ID, "answer").send_keys(result)
    browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click()

    final_result = browser.switch_to.alert.text.split(": ")[-1]
    print(public_result("https://stepik.org/lesson/181384/step/8?unit=156009", final_result))

finally:
    browser.quit()
