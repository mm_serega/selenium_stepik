import time
from selenium import webdriver
from selenium.webdriver.common.by import By
import math

def calc(x): # Функция для подсчета
    return str(math.log(abs(12*math.sin(int(x)))))

try:
    browser = webdriver.Chrome()
    browser.get("http://suninjuly.github.io/get_attribute.html")
    time.sleep(2)
    math_text = browser.find_element(By.ID, "treasure").get_attribute("valuex")#Находим число по ID и делаем его текстом(строкой)
    print(math_text)
    browser.find_element(By.ID, "answer").send_keys(calc(math_text)) # Находим по ID инпут и вводим в него значение, которое вернула функция
    browser.find_element(By.ID, "robotCheckbox").click() # Находим чек-бокс по ID и кликаем по нему
    browser.find_element(By.ID, "robotsRule").click() # Находим радиобаттон по ID и кликаем по нему
    browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click() # Находим кнопку по атрибуту и кликаем по нему

    time.sleep(10)
finally:
    browser.quit()

