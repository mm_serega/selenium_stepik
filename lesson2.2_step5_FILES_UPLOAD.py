import os
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
from Stepik_PUBLIC_RESULT import alert_result
from Stepik_PUBLIC_RESULT import public_result

current_dir = os.path.abspath(os.path.dirname(__file__)) # получаем путь к директории текущего исполняемого файла
file_path = os.path.join(current_dir, 'test.txt') # добавляем к этому пути имя файла

try:
    browser = webdriver.Chrome()
    link = "http://suninjuly.github.io/file_input.html"
    browser.get(link)
    browser.find_element(By.NAME, "firstname").send_keys("Serega")
    browser.find_element(By.NAME, "lastname").send_keys("Maslovskiy")
    browser.find_element(By.NAME, "email").send_keys("sergei787787787@gmail.com")
    browser.find_element(By.CSS_SELECTOR, "[id=\"file\"]").send_keys(file_path)

    # Прямой путь
    # select_file.send_keys("/Users/serega/PycharmProjects/Selenium_stepik/test.txt")

    browser.find_element(By.TAG_NAME, "[type=\"submit\"]").click()
    result = alert_result(browser.switch_to.alert.text)

finally:
    url = "https://stepik.org/lesson/228249/step/8?unit=200781"
    time.sleep(5)
    print(public_result(url, result))
    browser.quit()

