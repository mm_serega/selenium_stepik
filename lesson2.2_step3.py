import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

try:
    browser = webdriver.Chrome()
    browser.get("http://suninjuly.github.io/selects2.html")
    time.sleep(1)
    number1 = browser.find_element(By.ID, "num1").text
    number2 = browser.find_element(By.ID, "num2").text
    sum = int(number1) + int(number2)
    select = Select(browser.find_element(By.TAG_NAME, "select"))
    select.select_by_value(str(sum))
    browser.find_element(By.CSS_SELECTOR, "[type=\"submit\"]").click()
finally:
    time.sleep(10)
    browser.quit()

